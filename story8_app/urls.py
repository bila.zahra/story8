from django.urls import include, path
from . import views
from django.shortcuts import render
from django.conf.urls import url

app_name = 'story8_app'
urlpatterns = [
	path('status/', views.home,name='index'),
	path('books/', views.search_books,name='books'),
	path('', views.main,name='main')
]