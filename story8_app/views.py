from django.shortcuts import render, redirect
from .models import modelStatus
from .forms import formStatus

def home(request):
	status = modelStatus.objects.all()
	createStatus = formStatus(request.POST or None)
	context={
		'stats':status,
		'form_status':createStatus,
	}

	if request.method=='POST':
		if createStatus.is_valid():
			status.create(
				status = createStatus.cleaned_data.get('status'),
				time = createStatus.cleaned_data.get('time'),
				)
	return render(request,'status.html',context)

def main(request):
	return render(request,'main.html',{})

def search_books(request):
	return render(request,'search-books.html',{})
# Create your views here.
