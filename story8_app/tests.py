from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time
# Create your tests here.
class Lab6UnitTest(TestCase):
	def test_url_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code,200)
	
	def test_url_using_func(self):
		found = resolve('/status/')
		self.assertEqual(found.func, home)

	def test_create_models(self):
		new_model = modelStatus.objects.create(status ='PUSING')
		counting_new_model = modelStatus.objects.all().count()
		self.assertEqual(counting_new_model,1)

	def test_form_is_blank(self):
		form = formStatus(data= {'status':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
                form.errors['status'],
                ["This field is required."]
                )

	def test_form_template_used(self):
		response = Client().get('/status/')
		self.assertTemplateUsed(response, 'status.html')

	def test_post_form(self):
		response = Client().post('/status/',{
				'status':'hai'
			})
		self.assertIn('hai',response.content.decode())

	def test_views_books(self):
		found=resolve('/books/')
		self.assertEqual(found.func,search_books)


class Lab6FunctionalTest(LiveServerTestCase):
    
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-dev-shm-usage')
		ds = DesiredCapabilities.CHROME
		ds['loggingPrefs'] = {'browser': 'ALL'}
		self.selenium  = webdriver.Chrome(desired_capabilities=ds,chrome_options=chrome_options)
		super(Lab6FunctionalTest,self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab6FunctionalTest, self).tearDown()

	def test_input(self):
		selenium = self.selenium
		selenium.get('%s%s' % (self.live_server_url, '/status/'))
		status =selenium.find_element_by_name('status')
		submit =selenium.find_element_by_name('submit')

		message = 'PUSINGGGG'
		status.send_keys(message)
		submit.click()
		time.sleep(3)
		
		self.assertIn(message, selenium.page_source)